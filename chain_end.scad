difference() {
    union() {
        difference() {
            union() {
                color("red") {
                    import("lib/Open_Cable_ChainHypercube/CableChain_BaseUnit_2020End_v2.stl");
                }

                difference() {
                    translate([-5.7, 0, 0]) {
                        color("red") {
                            import("lib/Open_Cable_ChainHypercube/CableChain_BaseUnit_2020End_v2.stl");
                        }
                    }
                    translate([0, 5, 0]) {
                        color("orange") {
                            cube([30,10,10]);
                        } 
                    }
                }
                translate([4.3, 0, 0]) {
                    cube([6, 20, 4.5]);
                }
            }

            color("orange") {
                translate([17, -5, 10]) {
                    rotate([0, 45, 0]) {
                        translate([-10, 0, 0]) {
                            cube([30, 30, 20]);
                        }
                    }
                }
            }


            translate([24, 0, 0]) {
                color("green") {
                    cube([10, 20, 10]);
                }
            }
        }



        difference() {
            color("salmon") {
                translate([24, 20, 3]) {
                    rotate([0, 0, 180]) {
                        
                        import("lib/Open_Cable_ChainHypercube/CableChain_BaseUnit.stl");
                    }
                }
            }



            


            translate([0, 0, 0]) {
                color("orange") {
                    cube([10, 20, 10]);
                }
            }
            



                    color("cyan") {
                        translate([1, -5, 0]) {
                            cube([10, 30, 20]);
                        }
                    }



            color("orange") {
                translate([0, 5, 0]) {
                    cube([30, 10, 10]);
                }
            }

        }


        color("cyan") {
            translate([12, .2, 8]) {
                cube([5, 3, 5]);
            }
            translate([12, 16.8, 8]) {
                cube([5, 3, 5]);
            }
        }
        
    }



translate([-31.5, -1, 0]) {
    rotate([0, 45, 0]) {
        cube([20, 30, 50]);
    }
}

}