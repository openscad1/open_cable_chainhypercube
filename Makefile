ifeq ($(shell uname -s),Darwin)
	OPENSCAD := /Applications/OpenSCAD.app/Contents/MacOS/OpenSCAD
else ifeq ($(shell uname -s),Linux)
	OPENSCAD := /usr/bin/openscad
endif


.PRECIOUS: cache/%.stl cache/%-lores.stl cache/%-midres.stl cache/%-hires.stl

SHELL := bash

INDENT := 2>&1 | sed 's/^/    /'

make.targets :
	@echo "available Make targets:"
	@$(MAKE) -pRrq -f $(firstword $(MAKEFILE_LIST)) : 2>/dev/null \
	| awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' \
	| egrep -v '^make.targets$$' \
	| sed 's/^/    make    /' \
	| env LC_COLLATE=C sort


include $(shell find . -name '*.deps')


diag :
	uname -a
	uname -s
	env | sort

clean :
	find * -type f -name '*~' -exec rm -v {} \; ${INDENT}
	find * -type f -name '*.stl' -exec rm -v {} \; ${INDENT}
	find * -name '*.deps' -exec rm -v {} \; ${INDENT}
	([ -d artifacts ] && rmdir artifacts || true) ${INDENT}
	([ -d cache ] && rmdir cache || true) ${INDENT}

tidy :
	find artifacts -type f -name '*.stl' -exec rm -v {} \; ${INDENT}
	([ -d artifacts ] && rmdir artifacts || true) ${INDENT}

cache/%.stl : %.scad
	[ -d cache ] || mkdir -v cache
	date
	time ${OPENSCAD} -d $@.deps -o $@ $<
	date
	@echo .
	@echo .
	@echo .


cache/%-lores.stl : %.scad %.json
	[ -d cache ] || mkdir -v cache
	date
	time ${OPENSCAD} -d cache/$*.stl.deps -o $@ -p $*.json -P lores $<
	date
	@echo .
	@echo .
	@echo .



cache/%-midres.stl : %.scad %.json
	[ -d cache ] || mkdir -v cache
	date
	time ${OPENSCAD} -d cache/$*.stl.deps -o $@ -p $*.json -P midres $<
	date
	@echo .
	@echo .
	@echo .



cache/%-hires.stl : %.scad %.json
	[ -d cache ] || mkdir -v cache
	date
	time ${OPENSCAD} -d cache/$*.stl.deps -o $@ -p $*.json -P hires $<
	date
	@echo .
	@echo .
	@echo .




artifacts/%.stl : cache/%.stl
	@[ -d artifacts ] || mkdir -v artifacts
	cp $< $@

everything all : stl


allall : stl-lores stl-midres stl-hires


foo :
	for profile in *.json ; do \
		profile=$$(basename $$profile .json) ; \
		cat $${profile}.json | \
			jq -r '.parameterSets | keys[]' | \
			while read paramset ; do \
				echo doing $${paramset} for $${profile} ; \
			done ; \
	done


stl :
	@grep --files-without-match NOSTL *scad | while read s ; do \
		[ -r $$s ] && ${MAKE} --no-print-directory ${MAKEFLAGS} artifacts/$$(basename $$s .scad).stl ; \
	done

stl-% :
	@grep --files-without-match NOSTL *scad | while read s ; do \
		profile=$$(basename $$s .scad) ; \
		echo checking $$s against $${profile}.json ; \
		if [ -r $${profile}.json ] ; then \
			echo hooray ; \
			cat $${profile}.json | \
				jq -r '.parameterSets | keys[]' | fgrep $* | \
				while read paramset ; do \
					echo doing $${paramset} for $${profile} ; \
					${MAKE} --no-print-directory ${MAKEFLAGS} artifacts/$${profile}-$${paramset}.stl ; \
				done ; \
		fi ; \
	done

#EOF
